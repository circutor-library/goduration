# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/).


## 1.1.0 - 2023-09-05
- Added negative period feature

## 1.0.0 - 2023-09-05
- Initial release